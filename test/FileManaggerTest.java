import com.itextpdf.text.DocumentException;
import homeworks.task9_18_04_18.FileManagger;
import junitparams.JUnitParamsRunner;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@RunWith(JUnitParamsRunner.class)

public class FileManaggerTest {

    private final String PATH_TO_DIR = "./ForFileManager/";

    private FileManagger fileManagger = new FileManagger();

    @Rule
    public final ExpectedException ex = ExpectedException.none();

    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void shouldCreateDirectory() throws IOException {

        File newFolder = folder.newFolder("TestCreateDirectory");

        fileManagger.createDirectory(newFolder.getAbsolutePath());

    }

    @Test
    public void shouldCreateFile () throws IOException {

        fileManagger.createFile(PATH_TO_DIR + "testFile1.txt");

        Path path = Paths.get(PATH_TO_DIR + "testFile1.txt");

        Assert.assertTrue(Files.exists(path));

        Files.delete(path);

    }

    @Test
    public void shouldCopyFile () throws IOException {

        File newFolderSourceForCopy = folder.newFolder("Source");

        File newFolderTargetToCopy = folder.newFolder("Target");

        fileManagger.createFile(newFolderSourceForCopy.toString() + "/testFile1.txt");

        fileManagger.copyFile(newFolderSourceForCopy.toString() + "/testFile1.txt",
                                newFolderTargetToCopy.toString());

        Assert.assertTrue(Files.exists(Paths.get(newFolderSourceForCopy.toString() + "/testFile1.txt")));

    }

    @Test
    public void shouldDeleteDirectory() throws IOException {

        String pathDir = PATH_TO_DIR + "TestDirectory";

        fileManagger.createDirectory(pathDir);

        fileManagger.deleteDirectoryOrFile(pathDir);

        Assert.assertTrue(!(Files.exists(Paths.get(pathDir))));
    }

    @Test
    public void shouldDeleteFile() throws IOException {

        fileManagger.createFile(PATH_TO_DIR + "testFile1.txt");

        fileManagger.deleteDirectoryOrFile(PATH_TO_DIR + "testFile1.txt");

        Assert.assertTrue(!(Files.exists(Paths.get(PATH_TO_DIR + "testFile.txt"))));

    }

    @Test
    public void shouldRenameDirectory() throws IOException {

        fileManagger.createDirectory(PATH_TO_DIR + "TestDirectory1");

        fileManagger.renameDirectoryOrFile(PATH_TO_DIR + "TestDirectory1", "TestDirectory2");

        Assert.assertTrue(Files.exists(Paths.get(PATH_TO_DIR + "TestDirectory2")));

        fileManagger.deleteDirectoryOrFile(PATH_TO_DIR + "TestDirectory2");
    }

    @Test
    public void shouldRenameFile() throws IOException {

        fileManagger.createFile(PATH_TO_DIR + "testFile1.txt");

        fileManagger.renameDirectoryOrFile(PATH_TO_DIR + "testFile1.txt", "testFile2");

        Assert.assertTrue(Files.exists(Paths.get(PATH_TO_DIR + "testFile2.txt")));

        fileManagger.deleteDirectoryOrFile(PATH_TO_DIR + "testFile2.txt");

    }

    @Test
    public void shouldViewDirectory() throws IOException {

        File newFolders = folder.newFolder("Test1", "Test2", "Test3", "Test4");

        fileManagger.viewDirectory(newFolders.toString());

        Assert.assertTrue(outRule.getLog().contains("Test1/Test2/Test3/Test4"));

    }

    @Test
    public void shouldConvertFileToPDF () throws IOException, DocumentException {

        fileManagger.createFile(PATH_TO_DIR + "test1.txt");

        Path path = Paths.get(PATH_TO_DIR + "test1.txt");

        String text = "asd";

        Files.write(path, text.getBytes(), StandardOpenOption.CREATE);

        fileManagger.convertFileToPDF(PATH_TO_DIR + "test1.txt");

        Assert.assertTrue(Files.exists(Paths.get(PATH_TO_DIR + "test1.pdf")));

        fileManagger.deleteDirectoryOrFile(PATH_TO_DIR + "test1.pdf");

        System.out.println("GoodieYear");

    }


}
