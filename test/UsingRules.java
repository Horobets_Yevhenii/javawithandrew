import homeworks.task9_18_04_18.FileManagger;
import lessons.for_test.Calculator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

public class UsingRules {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test(expected = IOException.class)
    public void shouldThrowException() throws IOException {
        new Calculator().throwException();
    }

    @Test
    public void shouldThrowExceptionWithRule() throws IOException {

        exception.expect(IOException.class);

        exception.expectMessage("I'm IOE");

        new Calculator().throwException();
    }


    @Test
    public void shouldCreateDirs() throws IOException {
        File newFolder = folder.newFolder("USER", "Evgen");
        File file = folder.newFile("USER.txt");
        File file1 = folder.newFile("USER1.txt");
        FileManagger fm = new FileManagger();

     }
}
