import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UseMockito {

    @Mock
    Calculator calcMock;

    @Spy
    Calculator calcSpy;

    @Test
    public void shouldReturnSpecificValue() {

//        Calculator calcMock = Mockito.mock(Calculator.class);

        Mockito.when(calcMock.getValue()).thenReturn(55);

        Mockito.when(calcMock.diff()).thenCallRealMethod();

        Assert.assertEquals(55, calcMock.diff());
    }

    @Test
    public void shouldSpyClass() {
        //Calculator calcSpy = Mockito.spy(Calculator.class);

        Mockito.when(calcSpy.getValue()).thenReturn(55);

        Assert.assertEquals(55, calcSpy.diff());
    }
}
