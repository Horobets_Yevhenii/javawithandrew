import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import lessons.for_test.Calculator;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

//@RunWith(Parameterized.class)
@RunWith(JUnitParamsRunner.class)
public class CalculatorTest {

    @Parameterized.Parameter(value = 1)
    public int valueOne;

    @Parameterized.Parameter(value = 0)
    public int valueTwo;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {{9, -7}, {2, -8}, {-7, -3}};

        return Arrays.asList(data);
    }

    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("Before all tests");
    }

    @Before
    public void beforeEachTest() {
        System.out.println("Before each test");
    }

    @Test
    public void shouldReturnAddedValues() {
        Calculator calc = new Calculator();
        Assert.assertEquals("Should return 4", 4, calc.sum(2, 2));
        Assert.assertTrue(calc.getNumber());
    }

    @Test
    public void useParametrized() {
        Calculator calc = new Calculator();

        System.out.println(valueOne + " : " + valueTwo);

        Assert.assertEquals(valueOne + valueTwo, calc.sum(valueOne, valueTwo));
    }

    @Ignore
    @Test
    public void shouldReturnAddedValues1() {
        Calculator calc = new Calculator();
        Assert.assertEquals("Should return 4", 4, calc.sum(2, 2));
    }

    @Test
    public void checkConsole() {
        Calculator calc = new Calculator();

        calc.print();

        Assert.assertTrue(outRule.getLog().contains("Hello world"));
    }

    @Test
    @Parameters({"-8|5", "4|-2"})
    public void useJUnitParamsLib(int value1, int value2) {
        Calculator calculator = new Calculator();

        Assert.assertEquals(value1 + value2, calculator.sum(value1, value2));
    }


    @After
    public void afterEachTest() {
        System.out.println("After each test");
    }

    @AfterClass
    public static void afterAllTests() {
        System.out.println("After all tests");
    }
}
