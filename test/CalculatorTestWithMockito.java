import homeworks.task4.mockitoForTests.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorTestWithMockito {

    @Mock
    Calculator calculatorMock;

    @Spy
    Calculator calculatorSpy;

    @Test
    public void shouldAdditionalsOfNumbers () {

        Mockito.when(calculatorMock.generateNumber1()).thenReturn(10);

        Mockito.when(calculatorMock.generateNumber2()).thenReturn(5);

        Mockito.when(calculatorMock.additionOfNumbers()).thenCallRealMethod();

        Assert.assertEquals(15, calculatorMock.additionOfNumbers());
    }

    @Test
    public void shouldDivisionOfNumbers () {

        Mockito.when(calculatorSpy.generateNumber1()).thenReturn(20);

        Mockito.when(calculatorSpy.generateNumber2()).thenReturn(5);

        Assert.assertEquals(4, calculatorSpy.divisionOfNumbers());

    }

    @Test
    public void shouldMultiplicationOfNumbers () {

        Mockito.when(calculatorMock.generateNumber1()).thenReturn(2);

        Mockito.when(calculatorMock.generateNumber2()).thenReturn(2);

        Mockito.when(calculatorMock.multiplicationOfNumbers()).thenCallRealMethod();

        Assert.assertEquals(4, calculatorMock.multiplicationOfNumbers());
    }

    @Test
    public void shouldSubstractionOnNumber () {

        Mockito.when(calculatorSpy.generateNumber1()).thenReturn(10);

        Mockito.when(calculatorSpy.generateNumber2()).thenReturn(5);

        Assert.assertEquals(5, calculatorSpy.substractionOfNumbers());
    }
}
