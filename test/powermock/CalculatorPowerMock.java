package powermock;

import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Calculator.class)
public class CalculatorPowerMock {

    @Test
    public void shouldMockStaticMethod() {
        PowerMockito.mockStatic(Calculator.class);

        PowerMockito.when(Calculator.sumStatic()).thenReturn(8);

        Assert.assertEquals(8, Calculator.sumStatic());
    }

    @Test
    public void shouldMockPrivateMethod() throws Exception {
        Calculator spy = PowerMockito.spy(new Calculator());

        PowerMockito.doReturn(9).when(spy, "getRandomNumber");

        Assert.assertEquals(9, spy.newSum());
    }

    @Test
    public void shouldMockFinalMethod() {
        Calculator mock = PowerMockito.mock(Calculator.class);

        Mockito.when(mock.showNumber()).thenReturn(15);

        Assert.assertEquals(15, mock.showNumber());
    }
}
