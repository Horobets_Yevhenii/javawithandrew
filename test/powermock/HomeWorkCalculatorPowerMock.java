package powermock;

import homeworks.task4.mockitoForTests.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Calculator.class)
public class HomeWorkCalculatorPowerMock {

    @Test
    public void shouldMockFinalStaticMethod() {

        PowerMockito.mockStatic(Calculator.class);

        PowerMockito.when(Calculator.generateNumberFinalMethod()).thenReturn(5);

        Assert.assertEquals(5, Calculator.generateNumberFinalMethod());

    }

    @Test
    public void shouldMockPrivateMethod() throws Exception {

        Calculator spy = PowerMockito.spy(new Calculator());

        PowerMockito.doReturn(10).when(spy, "generateNumberPrivateMethod");

        Assert.assertEquals(10, spy.randomSum());

    }
}