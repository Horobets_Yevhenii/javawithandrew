import homeworks.task3.applicationForMeet.Gender;
import homeworks.task3.applicationForMeet.User;
import homeworks.task3.applicationForMeet.UserList;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.mockito.Mockito;
import org.mockito.Spy;

import java.util.Arrays;

public class UserListTest {

    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @Spy
    private static UserList list = new UserList(10);

    @BeforeClass
    public static void beforeEachTest() {
        User[] users = new User[5];
        users[0] = new User("Masha", "Ivanova", 20, "Moscow", 1, Gender.FEMALE);
        users[1] = new User("Misha", "Smith", 20, "Minsk", 2, Gender.MALE);
        users[2] = new User("Ira", "Smirnova", 20, "Kiev", 1, Gender.FEMALE);
        users[3] = new User("Kelly", "O'Neil", 22, "Boston", 2, Gender.FEMALE);
        users[4] = new User("Ted", "Johnson", 22, "Chicago", 2, Gender.MALE);
        list.setUsers(users);
    }

    @Test
    public void shouldAddUser() {

        User user = new User("Bob", "Dilan", 20, "Boston", 1, Gender.MALE);

        list.addUserWithFindingPartners(user);

        User[] users = list.getUsers();

        Assert.assertTrue(Arrays.asList(users).contains(user));
        Assert.assertTrue(outRule.getLog().contains("Masha"));
        Assert.assertTrue(outRule.getLog().contains("Ira"));

    }

    @Test
    public void shouldFindUserByNameAndSurname() {

        String name = "Masha";

        String surname = "Ivanova";

        list.findUserByNameAndSurname(name, surname);

        User[] users = list.getUsers();

        boolean isUser = false;

        for (User user : users) {
            if (user.getName().equals(name) && user.getSurname().equals(surname)) {
                isUser = true;
                break;
            }
        }

        Assert.assertTrue(isUser);

    }

    @Test
    public void shouldSearchWithAdvancedParameters() {

        String city = "Chicago";

        Gender gender = Gender.MALE;

        int age = 22;

        int childrenInFamily = 2;

        list.searchWithAdvancedParameters(city, gender, age, childrenInFamily);

        User[] users = list.getUsers();

        boolean isUser = false;

        for (User user : users) {
            if (user.getCity().equals(city) &&
                    user.getGender() == gender &&
                    user.getAge() == age &&
                    user.getChildrenInFamily() == childrenInFamily) {

                isUser = true;

                break;

            }
        }

        Assert.assertTrue(outRule.getLog().contains("Ted"));

    }

    @Test
    public void shouldViewAllRegisteredOppositeSex() {

        Gender gender = Gender.MALE;

        User[] users = list.getUsers();

        boolean isUser = false;

        for (User user : users) {

            if(user.getGender() != gender) {

                isUser = true;

                System.out.println(user);

            }
        }
        Assert.assertTrue(outRule.getLog().contains("Ira"));
        Assert.assertTrue(outRule.getLog().contains("Masha"));
        Assert.assertTrue(outRule.getLog().contains("Kelly"));
    }
}
