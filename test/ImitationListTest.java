import homeworks.task2.ImitationList;
import org.junit.*;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class ImitationListTest {
    ImitationList list = new ImitationList();

    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();

    @Before
    public void beforeEachTest() {
        list.setMyArray(new int[1]);
    }

    @Test
    public void shouldAddElement() {

        list.addElement(3);

        Assert.assertEquals(3, list.getMyArray()[0]);
    }

    @Test
    public void shouldBeIncreaseArray() {

        list.increaseArray(5);

        Assert.assertTrue(list.getMyArray().length == 6);
    }

    @Test
    public void shouldChangeElementByIndex() {
        list.changeElementByIndex(0,2);
        Assert.assertTrue(list.getMyArray()[0] == 2);
    }

    @Test
    public void shouldBeAddArray() {
        list.addArray(3);
        Assert.assertTrue(list.getMyArray().length == 3);
    }

    @Test
    public void shouldReductionArray() {
        list.setMyArray(new int[10]);
        list.reductionArray(3);
        Assert.assertTrue(list.getMyArray().length == 3);
    }

    @Test
    public void shouldBubbleSortArray() {
        list.setMyArray(new int[3]);
        list.addElement(37);
        list.addElement(53);
        list.addElement(13);
        list.bubbleSort();
        Assert.assertEquals(53, list.getMyArray()[2]);
    }

    @Test
    public void shouldDeleteElementByIndex() {//1 3
        list.setMyArray(new int[10]);

        list.deleteElementByIndex(2);

        Assert.assertTrue(list.getMyArray().length == 9);
        //add test on deleted element
    }

    @Test
    public void shouldRemoveDuplicates() {
        list.setMyArray(new int[5]);
        list.addElement(1);
        list.addElement(1);
        list.addElement(1);
        list.addElement(2);
        list.addElement(2);
        list.removeDuplicates();
        Assert.assertTrue(list.getMyArray().length == 2);
    }

    @Test
    public void shouldRemoveDuplicates2() {
        list.setMyArray(new int[5]);
        list.addElement(1);
        list.addElement(1);
        list.addElement(1);
        list.addElement(2);
        list.addElement(2);

        list.removeDuplicates2();

        Assert.assertTrue(list.getMyArray().length == 2);
    }

    @Test
    public void checkPrintArray() {
        list.setMyArray(new int[5]);
        list.addElement(3);
        list.addElement(7);
        list.addElement(4);
        list.addElement(5);
        list.addElement(11);
        list.printArray();
        Assert.assertTrue(outRule.getLog().contains("3 7 4 5 11"));
    }

    @Test
    public void checkPrintArrayReverse() {
        list.setMyArray(new int[5]);
        list.addElement(3);
        list.addElement(7);
        list.addElement(4);
        list.addElement(5);
        list.addElement(11);
        list.printArrayReverse();
        Assert.assertTrue(outRule.getLog().contains("11 5 4 7 3"));
    }
}
