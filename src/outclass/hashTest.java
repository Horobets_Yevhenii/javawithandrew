package outclass;

import java.util.*;

public class hashTest {

    public static void main(String[] args) {

        Map<String, Pet> hashMap = new HashMap<>();

        hashMap.put("Кот", new Cat("Мурзик"));
        hashMap.put("Собака", new Dog("Бобик"));
        hashMap.put("Попугай", new Parrot("Кеша"));
        System.out.println(hashMap);
        Pet cat = hashMap.get("Кот");
        System.out.println(cat);
        System.out.println(hashMap.containsKey("Кот"));
        System.out.println(hashMap.containsValue(cat));

        Map<Person, List<? extends Pet>> personMap = new HashMap<>();

        personMap.put(new Person("Ivan"), Arrays.asList(new Cat("Barsik"), new Cat("Murzik")));
        personMap.put(new Person("Masha"), Arrays.asList(new Cat("Vaska"), new Dog("Bobik")));
        personMap.put(new Person("Irina"), Arrays.asList(new Cat("Rizhik"), new Dog("Sharik"), new Parrot("Gosha")));

        System.out.println("personMap: " + personMap);
        System.out.println("personMap.keySet(): " + personMap.keySet());

        for (Person person : personMap.keySet()) {
            System.out.println(person + " имеет");
            for (Pet pet : personMap.get(person)) {
                System.out.println("  " + pet);
            }
        }
    }
}

class Person {

    String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Pet {
    String name;

     Pet(String name) {
        this.name = name;
    }

}

class Cat extends Pet{

    public Cat(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return  "Cat name " + name;
    }
}

class Dog extends Pet {

    public Dog(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return  "Dog name " + name;
    }
}

class Parrot extends Pet {

    public Parrot(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return  "Parrot name " + name;
    }
}
