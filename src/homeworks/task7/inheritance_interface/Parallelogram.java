package homeworks.task7.inheritance_interface;

public class Parallelogram extends Figure {

    public Parallelogram(String name, int length, int height) {
        super(name, length, height);
    }

    @Override
    public void calculateSquare() {

        System.out.println("It has area is " + getHeight() * getSideLength() + " square meters");
    }


    @Override
    public String toString() {
        return "Parallelogram";
    }


}
