package homeworks.task7.inheritance_interface;

import java.util.Random;

public class TestClass {
    public static void main(String[] args) {

        Rhombus rhombus = new Rhombus("Rhombus", new Random().nextInt(10) + 1,
                                                       new Random().nextInt(8) + 1);

        rhombus.printName();

        rhombus.calculateSquare();

        System.out.println();

        Triangle triangle = new Triangle("Triangle", new Random().nextInt(10) + 1,
                                                           new Random().nextInt(8) + 1);

        triangle.printName();

        triangle.calculateSquare();

        System.out.println();

        Parallelogram parallelogram = new Parallelogram("Parallelogram", new Random().nextInt(10) + 1,
                                                                               new Random().nextInt(8) + 1);

        parallelogram.printName();

        parallelogram.calculateSquare();

    }
}
