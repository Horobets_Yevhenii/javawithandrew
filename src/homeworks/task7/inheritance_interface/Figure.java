package homeworks.task7.inheritance_interface;

public abstract class Figure implements Calculatable {

    private String name;

    private int sideLength;

    private int height;

    public Figure(String name, int length, int height) {

        this.name = name;

        this.sideLength = length;

        this.height = height;
    }

    @Override
    public void calculateSquare() {

        System.out.println("It has area is " + height * sideLength + " square meters");
    }

    public String getName() {
        return name;
    }

    int getSideLength() {
        return sideLength;
    }

    int getHeight() {
        return height;
    }
}
