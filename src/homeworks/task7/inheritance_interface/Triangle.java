package homeworks.task7.inheritance_interface;

public class Triangle extends Figure {

    Triangle(String name, int length, int height) {

        super(name, length, height / 2);

    }

    @Override
    public void calculateSquare() {

        System.out.println("My area is " + getSideLength() * getHeight() / 2 + " square meters");

    }

    @Override
    public void printName() {

        System.out.println("Hi! My name is " + getName());

    }
}
