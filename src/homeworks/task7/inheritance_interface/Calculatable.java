package homeworks.task7.inheritance_interface;

public interface Calculatable {

    void calculateSquare ();

    default void printName () {

        System.out.println(this.getClass().getSimpleName());

    }
}
