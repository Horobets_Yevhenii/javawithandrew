package homeworks.task7.inheritance_interface;

public class Rhombus extends Figure {

    Rhombus(String name, int length, int height) {
        super(name, length, height);
    }

    @Override
    public void calculateSquare() {

        System.out.println("Its area is = " + getSideLength() * getHeight() + " square meters");

    }

    @Override
    public void printName() {

        System.out.println("It is name - " + getName());

    }
}
