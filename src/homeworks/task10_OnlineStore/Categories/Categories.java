package homeworks.task10_OnlineStore.Categories;

public enum Categories {

    COMPUTER("COMPUTER"),
    PHONE("PHONE"),
    ACCESSORIES ("ACCESSORIES");

    private String shortName;

    Categories(String shortName) { this.shortName = shortName; }

    public String getShortName() {
        return shortName;
    }
}
