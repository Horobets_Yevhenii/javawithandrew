package homeworks.task10_OnlineStore.Categories;

public enum Subcategories {

    LAPTOP("LAPTOP"),
    PC("PC"),
    MOUSE("MOUSE"),
    MOUSE_PAD("MOUSE PAD"),
    MOBILE_PHONE("MOBILE PHONE"),
    SMART_PHONE("SMART PHONE"),
    HEADPHONES("HEADPHONES");

    private String shortName;

    Subcategories(String shortName) { this.shortName = shortName; }

    public String getShortName() { return shortName; }
}
