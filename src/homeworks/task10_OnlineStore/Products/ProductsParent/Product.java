package homeworks.task10_OnlineStore.Products.ProductsParent;

import homeworks.task10_OnlineStore.Categories.Categories;
import homeworks.task10_OnlineStore.Categories.Subcategories;

import java.util.Comparator;

public class Product implements Comparable<Product>, Comparator<Product> {

        private String name;

        private String manufacturer;

        private int yearProduce;

        private Subcategories subcategory;

        private Categories category;

        public Product(String name,
                       String manufacturer,
                       int yearProduce,
                       Subcategories subcategory,
                       Categories category) {
                this.name = name;
                this.manufacturer = manufacturer;
                this.yearProduce = yearProduce;
                this.subcategory = subcategory;
                this.category = category;
        }

        public String getName() {
                return name;
        }

        public String getManufacturer() {
                return manufacturer;
        }

        public int getYearProduce() {
                return yearProduce;
        }

        public Subcategories getSubcategory() {
                return subcategory;
        }

        public Categories getCategory() {
                return category;
        }


        @Override
        public String toString() {

                return "name: " + name + "\n" +
                        "manufacturer: " + manufacturer + "\n" +
                        "year produce: " + yearProduce + "\n";
        }


        @Override
        public int compareTo(Product o) {

                return this.name.compareTo(o.name);

        }

        @Override//method from comparator
        public int compare(Product o1, Product o2) {

                Integer result;

                if ((o1.getName().compareTo(o2.getName())) < 0)

                        return result = -1;

                else if ((o1.getName().compareTo(o2.getName())) > 0)

                        return result = 1;

                else return result = 0;

        }
}