package homeworks.task10_OnlineStore;

import homeworks.task10_OnlineStore.ApplicationMethods.ApplicationMethods;
import homeworks.task10_OnlineStore.Categories.Categories;
import homeworks.task10_OnlineStore.Categories.Subcategories;
import homeworks.task10_OnlineStore.Products.ProductsParent.Product;

import java.util.Scanner;

public class TestApplication {

    static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        ApplicationMethods application = new ApplicationMethods();

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            try {
                switch (key) {
                    case 1: {

                        System.out.println("\nEnter subcategory:");

                        Subcategories subcategories = Subcategories.valueOf(SCANNER.next().toUpperCase());

                        System.out.println("Name:");
                        String name = SCANNER.next();

                        System.out.println("Manufacturer:");
                        String manufacturer = SCANNER.next();

                        System.out.println("Enter year:");
                        int yearProduce = SCANNER.nextInt();

                        System.out.println("Enter category:");
                        String categoryString = SCANNER.next();
                        Categories categories = Categories.valueOf(categoryString.toUpperCase());

                        application.addProduct(name, manufacturer, yearProduce, subcategories, categories);

                        break;
                    }
                    case 2: {

                        System.out.println("\nEnter subcategories:");

                        Subcategories subcategories = Subcategories.valueOf(SCANNER.next().toUpperCase());

                        System.out.println("Enter name product:");

                        String name = SCANNER.next();

                        System.out.println("Enter the manufacturer:");

                        String manufacturer = SCANNER.next();

                        System.out.println("Enter the year of production:");

                        int yearProduction = SCANNER.nextInt();

                        application.selectionProductWithCriteria(subcategories, name, manufacturer, yearProduction);

                        break;
                    }
                    case 3: {

                        System.out.println("Hi! You started create user. Enter name: ");

                        String nameUser = SCANNER.next();

                        System.out.println("Enter phone number: ");

                        int phoneNumber = SCANNER.nextInt();

                        application.createUserWithBasket(nameUser, phoneNumber);

                        break;
                    }

                    case 4: {

                        System.out.println("Enter name products:");

                        String name = SCANNER.next();

                        System.out.println("Enter manufacturer:");

                        String manufacturer = SCANNER.next();

                        application.deleteProductsWithRelatedGoods(name, manufacturer);

                        break;

                    }

                    case 5: {

                        application.viewContents();

                        break;

                    }

                    case 6: {

                        application.sortAscending();

                        break;

                    }

                    case 7: {

                        application.sortDescending();

                        break;

                    }
                    case 8:
                        return;
                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    private static void menu() {
        System.out.println("1) add product.\n" +
                "2) selection product with criteria.\n" +
                "3) create user.\n" +
                "4) deleted products with related goods.\n" +
                "5) view contens.\n" +
                "6) sort ascending.\n" +
                "7) sort descending.\n" +
                "8) Exit.");
    }
}
