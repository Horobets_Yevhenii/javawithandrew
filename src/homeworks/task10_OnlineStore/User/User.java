package homeworks.task10_OnlineStore.User;

import homeworks.task10_OnlineStore.Products.ProductsParent.Product;

import java.util.LinkedList;
import java.util.List;

public class User {

    private String nameUser;

    private final int phoneNumberID;

    private List<Product> list;

    public User(String nameUser, int phoneNumber) {
        this.nameUser = nameUser;
        this.phoneNumberID = phoneNumber;
        this.list = new LinkedList<>();
    }

    public String getNameUser() {
        return nameUser;
    }

    public int getPhoneNumber() {
        return phoneNumberID;
    }

    public List<Product> getList() {
        return list;
    }

    public void setList(List<Product> list) {

        this.list = list;

    }


    @Override
    public String toString() {
        return "Name: " + nameUser + "\n" +
                "ID: " + phoneNumberID;
    }
}
