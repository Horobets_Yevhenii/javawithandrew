package homeworks.task10_OnlineStore.ApplicationMethods;

import homeworks.task10_OnlineStore.Categories.Categories;
import homeworks.task10_OnlineStore.Categories.Subcategories;
import homeworks.task10_OnlineStore.Comparator.ProductNameComparator;
import homeworks.task10_OnlineStore.Products.ProductsParent.Product;
import homeworks.task10_OnlineStore.User.User;

import java.util.*;

public class ApplicationMethods {

    private List<Product> productsList = new LinkedList<>();

    //basic methods
    public void addProduct(String name,
                           String manufacturer,
                           int yearProduced,
                           Subcategories subcategories,
                           Categories categories) {

        productsList.add(createProduct(name, manufacturer, yearProduced, subcategories, categories));
    }

    public void selectionProductWithCriteria(Subcategories subcategories,
                                             String nameProduct,
                                             String manufacturer,
                                             int yearProduction) throws IndexOutOfBoundsException{

        Product selectionProduct = productsList.get(findProduct(subcategories, nameProduct, manufacturer, yearProduction));

        System.out.println(selectionProduct);

        outputRelatedProducts(selectionProduct);
    }

    public void createUserWithBasket (String nameUser, int phoneNumber) {

        User user = new User(nameUser, phoneNumber);

    }

    //почему я не могу удалить сам продукт в foreach?
    public void deleteProductsWithRelatedGoods(String nameProduct, String manufacturer) {

        Product selectedProduct = productsList.get(findProduct(nameProduct, manufacturer));

        deletedRelatedProduct(selectedProduct);

        productsList.remove(selectedProduct);
    }

    public void viewContents() {

        for (Product p: productsList) {

            System.out.println(p.toString());
        }

    }

    public void sortAscending() {

        Collections.sort(productsList);
    }

    public void sortDescending() {

        productsList.sort(new ProductNameComparator().reversed());

    }



    //additional methods

    private void deletedRelatedProduct(Product selectedProduct) {
        for (Product p: productsList) {

            if (p.getCategory().equals(selectedProduct.getCategory())) {

                productsList.remove(p);
            }

            else System.out.println("Good product");
        }
    }

    private Product createProduct(String name,
                               String manufacturer,
                               int yearProduce,
                               Subcategories subcategories,
                               Categories categories) {

        return new Product(name, manufacturer, yearProduce, subcategories, categories);



    }

    private void outputRelatedProducts(Product selectionProduct) {
        for (Product p: productsList) {

            if (selectionProduct.getCategory().equals(p.getCategory()) &&
                    (!(selectionProduct.getSubcategory().equals(p.getSubcategory())))) {

                System.out.println(p.toString());

            }
        }
    }

    private int findProduct(Subcategories subcategories,
                            String nameProduct,
                            String manufacturer,
                            int yearProduction ) throws IndexOutOfBoundsException {

        int indexSelectedProduct = -1;

        for (Product p: productsList) {


            if (p.getSubcategory().equals(subcategories) &&
                    p.getName().equalsIgnoreCase(nameProduct) &&
                    p.getManufacturer().equalsIgnoreCase(manufacturer) &&
                    p.getYearProduce() == yearProduction) {

                indexSelectedProduct = productsList.indexOf(p);

            }
        }

        return indexSelectedProduct;

    }

    private int findProduct(String nameProduct,
                            String manufacturer) throws IndexOutOfBoundsException {

        int indexSelectedProduct = -1;

        for (Product p : productsList) {

            if (p.getName().equalsIgnoreCase(nameProduct) && p.getManufacturer().equalsIgnoreCase(manufacturer)) {

                indexSelectedProduct = productsList.indexOf(p);

            }

        }

        return indexSelectedProduct;
    }


}
