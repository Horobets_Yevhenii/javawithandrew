package homeworks.task3.string;

import java.util.Scanner;

public class Search_long_and_short_words {

    private String longWord;
    private String shortWord;

    void searchLongWord (String sentence) {

        String[] arrayWords = sentence.split(" ");

        int maxWord = arrayWords[0].length();

        for (String s : arrayWords) {

            if (s.length() >= maxWord) {
                //maxWord = s.length();
                longWord = s;
            }
        }

        System.out.println("Long word in this sentence is - " + longWord);

    }

    void searchShortWord (String sentence) {

        String[] arrayWords = sentence.split(" ");

        int minWord = arrayWords[0].length();

        for (String d : arrayWords) {

            if (d.length() < minWord) {
                //minWord = d.length();
                shortWord = d;
            }
        }

        System.out.println("Short word in this sentence is - " + shortWord);

    }
}
