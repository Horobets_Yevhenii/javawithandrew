package homeworks.task3.string;

import java.util.Scanner;

public class Search_words {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        String str = sc.nextLine();

        String[] substrings = str.split(" ");

        int maxLength = 0;

        for(String s : substrings) {
            if(s.length() >= maxLength) {
                maxLength = s.length();
            }
        }

        System.out.println(maxLength);

    }
}

