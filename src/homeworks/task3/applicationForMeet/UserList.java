package homeworks.task3.applicationForMeet;

public class UserList {

    private User[] users;

    //TODO Only for testing
    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    public UserList(int size) {
        this.users = new User[size];
        users[0] = new User("Masha", "Ivanova", 20, "Moscow", 1, Gender.FEMALE);
        users[1] = new User("Misha", "Smith", 20, "Minsk", 2, Gender.MALE);
        users[2] = new User("Ira", "Smirnova", 20, "Kiev", 1, Gender.FEMALE);
        users[3] = new User("Kelly", "O'Neil", 22, "Boston", 2, Gender.FEMALE);
        users[4] = new User("Ted", "Johnson", 22, "Chicago", 2, Gender.MALE);

    }


    public void searchWithAdvancedParameters(String city, Gender userGender, int age, int childrenInFamily) {

        for (User user : users) {

            if (user != null &&

                    city.equalsIgnoreCase(user.getCity()) &&

                    userGender == user.getGender() &&

                    age == user.getAge() &&

                    childrenInFamily == user.getChildrenInFamily()) {

                System.out.println(user);;

                break;
            }
        }
    }

    public void findUserByNameAndSurname(String name, String surname) {

        for (User user : users) {

            if (user != null &&
                    name.equalsIgnoreCase(user.getName()) &&
                    surname.equalsIgnoreCase(user.getSurname())) {

                System.out.println(user);;

                break;

            }

        }

    }//

    public void addUserWithFindingPartners(User user) {

        resizeArray();

        for (int i = 0; i < users.length; i++) {

            if (users[i] == null) {
                users[i] = user;
                break;
            }
        }

        System.out.println("Suitable users for you:");

        findSuitablePartnersByAgeAndGender(user.getAge(), user.getGender());
    }//

    public void viewAllRegisteredOppositeSex(Gender gender) {

        for (User user : users) {

            if (user != null && gender != user.getGender()) {

                System.out.println(user);
            }
        }
    }//


    private void findSuitablePartnersByAgeAndGender(int age, Gender gender) {

        for (int i = 0; i < users.length; i++) {

            if (users[i] != null && gender != users[i].getGender() && age == users[i].getAge()) {

                System.out.println(users[i]);

            }
        }
    }//

    private void resizeArray() {

        if (countingOfZeroElements() == 0) {

            User[] operatingArray = new User[users.length * 2];

            for (int i = 0; i < users.length; i++) {
                operatingArray[i] = users[i];
            }

            users = operatingArray;
        }
    }

    private int countingOfZeroElements() {
        int count = 0;
        for (int i = 0; i < users.length; i++) {
            if (users[i] == null) {
                count++;
            }
        }
        return count;
    }
}

//domain(User) -> UserService <- TestClass
