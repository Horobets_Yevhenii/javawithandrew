package homeworks.task3.applicationForMeet;

public class User {

    private int age;

    private int childrenInFamily;

    private String name;

    private String surname;

    private String city;

    private Gender gender;

    public User(String name, String surname, int age,String city, int childrenInFamily, Gender gender) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.city = city;
        this.childrenInFamily = childrenInFamily;
        this.gender = gender;
    }


    public int getAge() {
        return age;
    }

    public int getChildrenInFamily() {
        return childrenInFamily;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCity() {
        return city;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (age != user.age) return false;
        if (childrenInFamily != user.childrenInFamily) return false;
        if (!name.equals(user.name)) return false;
        if (!surname.equals(user.surname)) return false;
        if (!city.equals(user.city)) return false;
        return gender == user.gender;
    }

    @Override
    public String toString() {
        return "{ name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", childrenInFamily=" + childrenInFamily +
                ", city='" + city + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
//    enum Gender {
//
//        MALE, FEMALE
//
//    }
