package homeworks.task3.applicationForMeet;

import java.util.Scanner;

public class Test {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        UserList users = new UserList(10);

        while (true) {

            menu();

            int key = SCANNER.nextInt();

            try {
                switch (key) {

                    case 1: {

                        int age = Integer.parseInt(getInfoFromUser("Enter your age:"));

                        if (age < 18) {

                            System.out.println("Sorry, you very small");

                            break;
                        }

                        String name = getInfoFromUser("Enter your name");

                        String surname = getInfoFromUser("Enter your surname:");

                        String city = getInfoFromUser("Enter your city:");

                        int childrenInFamily = Integer.parseInt(getInfoFromUser("Enter the number of your children"));

                        String gender = getInfoFromUser("Enter your male(Male/Female):");

                        Gender userGender = Gender.valueOf(gender.toUpperCase());

                        System.out.println("____________________________________");

                        User user = new User(name, surname, age, city, childrenInFamily, userGender);

                        users.addUserWithFindingPartners(user);

                        break;
                    }

                    case 2: {

                        String gender = getInfoFromUser("Enter your gender:");

                        Gender userGender = Gender.valueOf(gender.toUpperCase());

                        users.viewAllRegisteredOppositeSex(userGender);

                        break;
                    }

                    case 3: {

                        System.out.println("Enter name:");

                        String name = SCANNER.next();

                        System.out.println("Enter surname");

                        String surname = SCANNER.next();

                        users.findUserByNameAndSurname(name, surname);

                        break;
                    }

                    case 4: {

                        System.out.println("Enter city:");

                        String city = SCANNER.next();

                        if (city == null) {

                            System.out.println("Enter city again, please");

                            break;

                        }

                        System.out.println("Enter gender(MALE, FEMALE):");

                        String gender = SCANNER.next();

                        if (gender == null) {

                            System.out.println("Enter gender again, please");

                            break;

                        }

                        Gender userGender = Gender.valueOf(gender.toUpperCase());

                        System.out.println("Enter age:");

                        int age = SCANNER.nextInt();

                        if (age == 0) {

                            System.out.println("Enter age again, please");

                            break;

                        }

                        System.out.println("Enter the number of children");

                        int childrenInFamily = SCANNER.nextInt();

                        users.searchWithAdvancedParameters(city, userGender, age, childrenInFamily);

                        break;
                    }

                    case 5:
                        return;
                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    private static void menu() {
        System.out.println(
                "1) Register on site.\n" +
                        "2) View all regisered opposite sex\n" +
                        "3) Find on name and surname.\n" +
                        "4) Advanced search.\n" +
                        "5) Exit.");
    }

    private static String getInfoFromUser(String message) {
        System.out.println(message);

        return SCANNER.next();
    }
}
