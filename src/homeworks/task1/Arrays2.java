package homeworks.task1;
//1)        Создать два массива из 30 чисел. Первый массив проинициализировать нечетными числами.
//        Проинициализировать второй массив элементами первого массива при условии,
//        что индекс элемента больше 4 и делится без остатка на 5 и элемент больше 0, но меньше 6 или больше 10, но меньше 20.
//        Если условие не выполняется оставить элемент массива без изменения.


import java.util.Random;

public class Arrays2 {
    public static void main(String[] args) {

        Random rand = new Random();
        int pairCount = 0;

        int indexAr1;

        int length = 30;

        int[] ar1 = new int[length];
        int[] ar2 = new int[length];

//        int[] ar1 = {1, 2, 5, 6, 5, 11, 13, 14, 15, 19, 21, 1, 3, 5, 7, 5, 11, 13, 15, 17, 19, 21, 1, 3, 5, 7, 5, 11, 13, 15};

        for (int i = 0; i < length; i++) {
            System.out.print(i + "\t");
        }

        System.out.println();

        for (indexAr1 = 0; indexAr1 < length; indexAr1++) {
            //ar1[indexAr1] = indexAr1 * 2 + 1;
            //ar1[indexAr1] = rand.nextInt(5) * 2 + 1;
            ar1[indexAr1] = rand.nextInt(5) << 1 + 1;
            System.out.print(ar1[indexAr1] + "\t");
        }

        System.out.println();

        //System.out.println((int) Math.pow(2, 4));
        //System.out.println(1 << 4); //1 * 2(4)


        for (indexAr1 = 0; indexAr1 < length; indexAr1++) {
            if (indexAr1 > 4 && indexAr1 % 5 == 0 &&
                    ((ar1[indexAr1] > 0 && ar1[indexAr1] < 6) ||
                    (ar1[indexAr1] > 10 && ar1[indexAr1] < 20))) {
                    ar2[indexAr1] = ar1[indexAr1];
            }

            System.out.print(ar2[indexAr1] + "\t");
            //if(true && false && false || true)
        }

    }
}