package homeworks.task1;

//4) В двумерном массиве натуральных случайных чисел от 10 до 99 найти количество всех двухзначных чисел,
//   у которых сумма цифр кратная 2.

import java.util.Random;

public class Arrays5 {
    public static void main(String[] args) {
        Random rand = new Random();
        int count = 0;
        int[][] ar1 = new int[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                ar1[i][j] = (rand.nextInt(89) + 10);
                System.out.print(ar1[i][j] + " ");
            }
            System.out.println();
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                int a = ar1[i][j] / 10;
                int b = ar1[i][j] - a * 10;
                if ((a + b) % 2 == 0) {
                    count++;
                }
            }
        }
        System.out.println(count);
    }
}
