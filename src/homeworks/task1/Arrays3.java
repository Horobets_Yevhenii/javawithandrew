package homeworks.task1;

//2)        Выяснить, имеются ли в данном массиве два идущих подряд  положительных элемента. Подсчитать количество таких пар.

public class Arrays3 {
    public static void main(String[] args) {
        int pairCount = 0;

        int[] ar1 = {1, 2, 5, 6, 5, 11, 13, 14, 15, 19, 21, 1, 3, 5, 7, 5, 11, 13, 15, 17, 19, 21, 1, 3, 5, 7, 5, 11, 13, 15};

        for (int indexAr1 = 0; indexAr1 < ar1.length - 1; indexAr1 ++) {
            if (ar1[indexAr1] > 0 && ar1[indexAr1 + 1] > 0 && (ar1[indexAr1] - ar1[indexAr1 + 1] == -1)) {
                pairCount++;
            }
        }

        System.out.print(pairCount + " ");
    }
}
