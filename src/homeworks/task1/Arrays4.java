package homeworks.task1;

//3) Дан двумерный массив целых чисел. Вычислить сумму элементов первой и последней строк данной матрицы.
//5) Вывести максимальный элемент каждой строки
//6) Необходимо найти и вывести наибольший, наименьший элемент, сумму всех элементов каждого столбца.

import java.util.Random;

public class Arrays4 {
    public static void main(String[] args) {
        Random rand = new Random();

        int row = 5;

        int column = 5;

        int[][] ar1 = new int[row][column];
        //array filling
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                ar1[i][j] = rand.nextInt(50);
                System.out.print(ar1[i][j] + " ");
            }
            System.out.println();
        }

        //first string summ
        int summFirstString = 0;

        int summLastString = 0;

        for (int i = 0; i < row; i++) {
            summFirstString += ar1[0][i];
            summLastString += ar1[row - 1][i];
        }

        System.out.println("First string summ = " + summFirstString +
                "\nLast string summ = " + summLastString);

        //max/min element of a string
        int max = 0;

        int min = 0;

        for (int i = 0; i < row; i++) {

            max = ar1[i][0];
            min = ar1[i][0];

            for (int j = 0; j < row; j++) {
                if (max < ar1[i][j]) {
                    max = ar1[i][j];
                }

                if (min > ar1[i][j]) {
                    min = ar1[i][j];
                }
            }

            System.out.println("Max value in the string " + i + " = " + max);
            System.out.println("Min value in the string " + i + " = " + min);
        }

        //max/min element of a column and their summ
        int summ = 0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {

                summ += ar1[j][i];

                if (max < ar1[j][i]) {
                    max = ar1[j][i];
                }

                if (min > ar1[j][i]) {
                    min = ar1[j][i];
                }
            }

            System.out.println("Summ = " + summ);
            System.out.println("Max value in the column " + i + " = " + max);
            System.out.println("Min value in the column " + i + " = " + min);

            summ = 0;
            max = 0;
            min = ar1[0][i];
        }
    }
}