package homeworks.task8.march22_03_18;

abstract class WriterReader implements Writable, Readable {

    public void modifyText() {

        System.out.println("I'm ready writing to file!");

    }

}
