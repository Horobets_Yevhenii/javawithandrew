package homeworks.task8.march22_03_18;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class HeirWriterReader extends WriterReader {

    Scanner scanner = new Scanner(System.in);

    public HeirWriterReader() {

        super.modifyText();

    }

    @Override
    public void modifyText() {
        System.out.println("I'm ready to read!");
    }

    @Override
    public void read(File file) {

        System.out.println("I'm read from file: " + file.getName());

        try (FileReader reader = new FileReader(file.getPath())) {

            int n;

            while ((n = reader.read()) != -1) {

                System.out.print((char)n);
            }

        }

        catch (IOException ex) {

            System.out.println(ex.getMessage());

        }

    }

    @Override
    public void write(File file) {

        System.out.println("I'm ready for writing to file");

        try (FileWriter writer = new FileWriter(file.getPath())) {

            String text = scanner.nextLine();

            writer.write(text);

            writer.flush();

        }
        catch (IOException ex) {

            System.out.println(ex.getMessage());
        }

        }
}
