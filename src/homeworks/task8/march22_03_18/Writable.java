package homeworks.task8.march22_03_18;

import java.io.File;

interface Writable {

    void write(File file);

}
