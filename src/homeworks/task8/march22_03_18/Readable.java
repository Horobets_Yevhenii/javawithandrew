package homeworks.task8.march22_03_18;

import java.io.File;

public interface Readable {

    void read(File file);

}
