package homeworks.task8Part2.writer;

import java.io.File;
import java.io.IOException;

public interface Writable {

    void write(String text) throws IOException;

}
