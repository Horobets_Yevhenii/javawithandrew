package homeworks.task8Part2.writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class HeirWriter extends Writer {

    public static final String FILE_NAME = "./Poem.txt";


    public void write(String text) throws IOException {

        Path path = Paths.get(FILE_NAME);

        //FileSystems.getDefault().getPath(FILE_NAME);

        text = modifyText(text);

        Files.write(path, text.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }
}
