package homeworks.task8Part2.reader;


import java.io.IOException;

public interface Readable {

    void read() throws IOException;

}
