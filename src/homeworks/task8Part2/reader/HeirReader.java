package homeworks.task8Part2.reader;


import homeworks.task8Part2.writer.HeirWriter;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class HeirReader extends Reader {

    public void read() throws IOException {

        Path path = Paths.get(HeirWriter.FILE_NAME);

        StringBuilder text = new StringBuilder();

        String line = null;

        try(BufferedReader reader = Files.newBufferedReader(path)) {

            while (Objects.nonNull(line = reader.readLine())) {
                text.append(line);
            }

        }

        line = modifyText(text.toString());

        System.out.println(line);

    }
}
