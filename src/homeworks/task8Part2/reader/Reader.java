package homeworks.task8Part2.reader;

abstract class Reader implements Readable {

    String modifyText(String text) {

        return text.replaceAll("I'm ready for writting to file", "I'm from file");
    }

}
