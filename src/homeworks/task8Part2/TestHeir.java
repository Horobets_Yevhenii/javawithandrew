package homeworks.task8Part2;

import homeworks.task8Part2.reader.HeirReader;
import homeworks.task8Part2.writer.HeirWriter;

import java.io.IOException;

public class TestHeir {
    public static void main(String[] args) throws IOException {

        HeirWriter heirWriter = new HeirWriter();

        HeirReader heirReader = new HeirReader();

        heirWriter.write("Hello, Eugeniy");

        heirReader.read();

    }
}
