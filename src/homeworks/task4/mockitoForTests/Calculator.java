package homeworks.task4.mockitoForTests;

import java.util.Random;

public class Calculator {

    public int additionOfNumbers () {
        int a = generateNumber1();
        int b = generateNumber2();
        return a + b;
    }

    public int multiplicationOfNumbers () {
        int a = generateNumber1();
        int b = generateNumber2();
        return a * b;
    }

    public int divisionOfNumbers () {
        int a = generateNumber1();
        int b = generateNumber2();
        return a / b;
    }

    public int substractionOfNumbers () {
        int a = generateNumber1();
        int b = generateNumber2();
        return a - b;
    }

    public int generateNumber1 () {
        return new Random().nextInt(50);
    }

    public int generateNumber2 () {
        return new Random().nextInt(30);
    }



    public int randomSum () {

        return generateNumberPrivateMethod();
    }

    private int generateNumberPrivateMethod () {

        return new Random().nextInt();
    }

    public final static int generateNumberFinalMethod () {

        return new Random().nextInt();

    }

}
