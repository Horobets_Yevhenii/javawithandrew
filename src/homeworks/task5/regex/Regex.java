package homeworks.task5.regex;

/*
2) Любое количество букв, а потом 2 цифры
3) Любое количество цифр, а потом много букв
4) Пользователь вводит имя и фамилию. Проверить, что первые буквы в верхнем регистре, а остальные в нижнем.
 */

import java.util.Scanner;
import java.util.regex.Pattern;

public class Regex {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        ///String firstTask = "[A-za-z]*[0-9]{2}";

        String firstTask = "\\D*\\d{2}";

        String textOne = "hellofrom94";

        System.out.println(Pattern.matches(firstTask, textOne));

        String secondTask = "[0-9]*[A-Za-z]*";

        String textTwo = "12345678910111213141516171819abcdefghijklmnopqrstuvwxyz";

        System.out.println(Pattern.matches(secondTask, textTwo));

        String name = "Bob";

        String nameCheck = "[A-Z]{1}[a-z]*";

        String surname = "Dilan";

        String surnameCheck = "[A-Z]{1}[a-z]*";

        System.out.println(Pattern.matches(nameCheck, name));

        System.out.println(Pattern.matches(surnameCheck, surname));








    }
}
