package homeworks.task2;

import java.io.IOException;

interface ReaderTofLog {

    void readLogs();

    void readLogsWithFilter(String data, String regex) throws IOException;
}
