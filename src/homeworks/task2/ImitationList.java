package homeworks.task2;

//Создать имитацию коллекции(с помощью массива) ArrayList для работы с типом int.
//
//Создать класс с полем типа массив.
//
//Класс должен выполнять следующие операции:
//
//1) добавление элементов.
//2) изменение/удаление элементов по индексу.
//3) увеличение листа на заданное количество элементов.
//4) уменьшение листа до заданного количество элементов.
//5) вывод элементов в консоль в прямом и обратном порядке.
//6) сортировка листа методом пузырька (http://study-java.ru/uroki-java/urok-11-sortirovka-massiva/)
//6a) поиск элемента методом бинарного и линейного поиска, quickSort---
// - сначала отсортировать массив.
// - потом делением 2 на два искать нужный элемент
//7) добавление массива в массив.
//8) удалять дубликаты.
//
//При удалении элемента не обнулять его, а удалять физически.
//Начальную размерность листа юзер вводит с консоли.
//Создать меню для работы с листом из консоли.
//Условие добавления: перезаисывать если элемент равен 0:

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.regex.Pattern;

public class ImitationList implements ReaderTofLog {


    private static final Path PATH_TO_FILE = Paths.get("./LOGS.txt");

    private int[] myArray;

    private LocalDateTime localDateTime = LocalDateTime.now();

    public ImitationList() {}

    ImitationList(int size) {

        this.myArray = new int[size];

    }

    //TODO Only for testing
    public int[] getMyArray() {
        return myArray;
    }

    //TODO Only for testing
    public void setMyArray(int[] myArray) {
        this.myArray = myArray;
    }

    public void increaseArray(int element) {

        String log = localDateTime + "\t increaseArray\t param = " + element;

        writeLog(log);

        int[] operatingArray = new int[myArray.length + element];

        for (int i = 0; i < myArray.length; i++) {
            operatingArray[i] = myArray[i];
        }

        myArray = operatingArray;
    }

    public void reductionArray(int element) {

        String log = localDateTime + "\t reductionArray\t param = " + element;

        writeLog(log);

        int[] operatingArray = new int[element];

        for (int i = 0; i < operatingArray.length; i++) {
            operatingArray[i] = myArray[i];
        }

        myArray = operatingArray;
    }

    public void bubbleSort() {

        String log = localDateTime + "\t bubbleSort";

        writeLog(log);

        for (int i = myArray.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (myArray[j] > myArray[j + 1]) {
                    int tmp = myArray[j];
                    myArray[j] = myArray[j + 1];
                    myArray[j + 1] = tmp;
                }
            }
        }
    }

    public void addElement(int element) {

        String log = localDateTime + "\t addElement\t param = " + element;

        writeLog(log);

        doubleTheArray();

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == 0) {
                myArray[i] = element;
                break;
            }
        }
    }

    public void changeElementByIndex(int index, int newElement) {

        String log = localDateTime + "\t changeElementByIndex\t param = index:[" + index + "]; new element:[" + newElement;

        writeLog(log);

        myArray[index] = newElement;
    }

    public void deleteElementByIndex(int index) {

        String log = localDateTime + "\t deleteElementByIndex\t param = " + index;

        writeLog(log);

        int[] temp = new int[myArray.length - 1];

        for (int i = 0; i < index; ++i) {
            temp[i] = myArray[i];
        }

        for (int i = index; i < myArray.length - 1; ++i) {
            temp[i] = myArray[i + 1];
        }
        myArray = temp;
    }

    public void printArray() {

        String log = localDateTime + "\t printArray";

        writeLog(log);

        for (int i = 0; i < myArray.length; i++) {
            System.out.print(myArray[i] + " ");
        }
    }

    public void printArrayReverse() {

        String log = localDateTime + "\t printArrayReverse";

        writeLog(log);

        for (int i = myArray.length - 1; i >= 0; i--) {
            System.out.print(myArray[i] + " ");
        }
    }

    public void addArray(int element) {

        String log = localDateTime + "\t addArray\t param = " + element;

        writeLog(log);

        int count = 0;

        int[] newArray = new int[element];

        int countNull = countingOfZeroElements();

        int newSize = newArray.length - countNull;

        if (countNull < newArray.length) {
            increaseArray(newSize);
        }

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == 0) {
                myArray[i] = newArray[count];
                count++;
            }
        }
    }

    public void removeDuplicates () {

        String log = localDateTime + "\t removeDuplicates";

        writeLog(log);

        bubbleSort();

        int[] operatingArray = new int[getCountUniqueValue()];

        int k = 0;

        if (operatingArray.length > 0) {
            operatingArray[k++] = myArray[0];
        }

        for (int i = 1; i < myArray.length; i ++) {
            if (myArray[i] != myArray[i - 1]) {
                operatingArray[k++] = myArray[i];
            }
        }

        myArray = operatingArray;
    }

    public void removeDuplicates2 () {

        String log = localDateTime + "\t removeDuplicates2";

        writeLog(log);

        for (int i = 0; i < myArray.length; i++) {

            int element = myArray[i];

                for (int j = i + 1; j < myArray.length; j++) {
                if (myArray[j] == element) {
                    deleteElementByIndex(j);
                    --j;
                }
            }
        }
    }

    private int countingOfZeroElements() {

        String log = localDateTime + "\t countingOfZeroElements";

        writeLog(log);

        int count = 0;
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == 0) {
                count++;
            }
        }
        return count;
    }

    private int getCountUniqueValue() {

        String log = localDateTime + "\t getCountingUniqueValue";

        writeLog(log);

        int countUniqueValue = 1;

        for (int i = 1; i < myArray.length; i ++) {
            if (myArray[i] != myArray[i - 1]) {
                countUniqueValue++;
            }
        }
        return countUniqueValue;
    }

    private void doubleTheArray() {

        String log = localDateTime + "\t doubleTheArray";

        writeLog(log);

        if (countingOfZeroElements() == 0) {

            int[] operatingArray = new int[myArray.length * 2];

            for (int i = 0; i < myArray.length; i++) {
                operatingArray[i] = myArray[i];
            }

            myArray = operatingArray;
        }
    }

    private void writeLog(String log) {
        try {
            Files.write(PATH_TO_FILE, (log + ";\n").getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void readLogs() {

        String line;

        Path path = Paths.get(PATH_TO_FILE.toString());

        try(BufferedReader reader = Files.newBufferedReader(path)) {

            while (Objects.nonNull(line = reader.readLine())) {

                System.out.println(line);
            }
        } catch (IOException e) { }

    }

    @Override
    public void readLogsWithFilter(String data, String regex) throws IOException {

        String line;

        try(BufferedReader reader = Files.newBufferedReader(PATH_TO_FILE)) {

            while (Objects.nonNull(line = reader.readLine())) {

                if (Pattern.matches(regex, line.toUpperCase())) {

                    System.out.println(line);

                }
            }
        }

    }
}
