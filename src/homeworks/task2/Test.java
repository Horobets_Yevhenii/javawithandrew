package homeworks.task2;

import java.util.Scanner;

public class Test {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Enter the size of the array: ");

        int size = SCANNER.nextInt();

        ImitationList myArray = new ImitationList(size);

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            try {
                switch (key) {
                    case 1: {//+++
                        System.out.println("Enter element that you want to add to the array, please:");

                        int element = SCANNER.nextInt();

                        myArray.addElement(element);

                        myArray.printArray();

                        break;
                    }
                    case 2: {//+++
                        System.out.println("Enter the index of the item you want to change, please: ");

                        int indexOfElementsToChange = SCANNER.nextInt();

                        System.out.println("Enter the number: ");

                        int newElement = SCANNER.nextInt();

                        myArray.changeElementByIndex(indexOfElementsToChange, newElement);

                        myArray.printArray();

                        break;
                    }
                    case 3: {//+++

                        System.out.println("Enter the number of elements, to which you want to increase the array, please: ");

                        int numberElementsToIncrease = SCANNER.nextInt();

                        myArray.increaseArray(numberElementsToIncrease);

                        myArray.printArray();

                        break;
                    }

                    case 4: {//+++

                        System.out.println("Enter the number of elements, to which you want to reduce the array: ");

                        int numberElementsToReduce = SCANNER.nextInt();

                        myArray.reductionArray(numberElementsToReduce);

                        myArray.printArray();

                        break;
                    }


                    case 5: {//+++

                        myArray.printArray();

                        break;
                    }

                    case 6: {//+++

                        myArray.printArrayReverse();

                        break;
                    }

                    case 7: {//+++

                        myArray.bubbleSort();

                        myArray.printArray();

                        break;
                    }

                    case 8: { //------------------------------------

                        System.out.println("Enter the number of elements of the array that you want to add to the current array: ");

                        int numberElementsInNewArray = SCANNER.nextInt();

                        myArray.addArray(numberElementsInNewArray);

                        myArray.printArray();

                        break;
                    }

                    case 9: {//+++

                        myArray.removeDuplicates();

                        myArray.printArray();

                        break;
                    }

                    case 10: {//+++

                        System.out.println("Enter the index of the item you want to delete: ");

                        int removableItem = SCANNER.nextInt();

                        myArray.deleteElementByIndex(removableItem);

                        myArray.printArray();

                        break;
                    }


                    case 11: {

                        System.out.println("Fill array, please. Enter elements: ");


                        myArray.printArray();

                        break;
                    }

                    case 12: {//+++

                        myArray.removeDuplicates2();

                        myArray.printArray();

                        break;
                    }

                    case 13:
                        return;
                    default:
                        return;

                    case 14: {

                        myArray.readLogs();

                        break;
                    }

                    case 15: {

                        System.out.println("Choose the filter:\n1 - Date filter\n2 - Name filter");

                        int numberFilter = SCANNER.nextInt();

                        if (numberFilter == 1) {

                            System.out.println("Enter date in format yyyy-mm-dd: ");

                            String date = SCANNER.next();

                            String regex = date + ".*";

                            myArray.readLogsWithFilter(date, regex);

                        }
                        else {

                            System.out.println("Enter the name:");

                            String name = SCANNER.next();

                            String regex = ".*" + name + ".*";

                            myArray.readLogsWithFilter(name, regex);

                        }

                        break;
                    }
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

//    private void fillArray(int size) {
//        for (int i = 0; i < size; i++) {
//            System.out.println("Enter " + i + " elements");
//            myArray.addElement(SCANNER.nextInt());
//        }
//    }

    private static void menu() {
        System.out.println(
                /* TEST + */"\n1) Add element.\n" +//++
                /* TEST + */"2) Change element by index\n" +//++
                /* TEST + */"3) Increase array\n" +//++
                /* TEST + */"4) Reduction array\n" +//++
                /* TEST + */"5) Print array in right order\n" +//++
                /* TEST + */"6) Print array in reverse order\n" +//++
                /* TEST + */"7) Bubble sort\n" +//++
                /* TEST + */"8) Add array\n" +//Как заполнить массив вручную??? Один и второй?!
                /* TEST + */"9) Remove duplicates\n" +//++
                /* TEST + */"10) Delete element by index\n" +//++
                            "11) Fill array\n" + //-------
                /* TEST + */"12) Remove duplicates 2\n" + //++
                            "13) Exit.\n" + //++
                            "14) Read all logs\n" +
                            "15) Read logs with filter.\n");
    }
}


//    public static void main(String[] args) {
//        System.out.println("Enter number of arrays");
//        ImitationList myArray = new ImitationList(SCANNER);
//        myArray.fillArray(SCANNER);
//        myArray.addElement(SCANNER);
//        myArray.increaseArray(SCANNER);
//        myArray.printArray();
//        myArray.bubbleSort();
//        myArray.reductionArray(SCANNER);
//        myArray.changeElementByIndex(SCANNER);
//        myArray.deleteElementByIndex(SCANNER);
//        myArray.addArray(SCANNER);
 //        myArray.removeDuplicates();
//        myArray.printArray();
//        myArray.printArrayReverse();