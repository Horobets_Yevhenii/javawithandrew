package homeworks.task6;

enum CodeOperator {

        MTS_1("(095)"),//new CodeOperator("(095)")
        MTS_2("(050)"),
        MTS_3("099"),
        LIFE_1("063"),
        LIFE_2("(093)"),
        LIFE_3("(073)"),
        KIEVSTAR_1("(067)"),
        KIEVSTAR_2("(097)"),
        KIEVSTAR_3("(098)");

        private String code;

        CodeOperator(String code) {
            this.code = code;
        }

        public String getCode() {

            return code;
        }
    }


