package homeworks.task6;
/*      4) В строке содержутся слова и числа. Необходмо выделить числа и посчитать их сумму.+
        5) Пользователь вводит телефон в формате (ххх)ххх-хх-хх. - optinal
        (ххх) может быть таким: (095), (097), (073), (067), (099), (063).
        Проверить, чтобы 7 - ый номер был в формате "(ххх)ххх-хх-хх".
        Если пользователь вводит номер начинающийся на
        (095) или (099) ххх-хх-хх, выводить в консоль "Пользователь имеет МТС номер",на
        (097) или (067) ххх-хх-хх, выводить в консоль "Пользователь имеет Киевстар номер",на
        (073) или (063) ххх-хх-хх, выводить в консоль "Пользователь имеет Лайф номер".*/


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex2 {

    public static void main(String[] args) {

        String text = "1 1hello my23 dear34 friend345";

        String patternDigit = "[0-9]";

        Pattern pattern = Pattern.compile(patternDigit);

        Matcher matcher = pattern.matcher(text);

        int sum = 0;

        while (matcher.find()) {

            int number = Integer.parseInt(matcher.group());

            sum += number;
        }

        System.out.println(sum);
    }
}

class PhoneNumber {

    static final String NUMBER_FORMAT = "(\\(095\\)|\\(099\\)|\\(050\\)|" +
                                        "\\(063\\)|\\(093\\)|\\(073\\)|" +
                                        "\\(067\\)|\\(097\\)|\\(098\\))" +
                                        "\\d{3}-\\d{2}-\\d{2}";

    void findMatches(String phoneNumber)  {

        if(!checkPhone(phoneNumber)) {
            System.out.println("Incorrect number");
            return;
        }

        String[] code = Pattern.compile("\\d{3}-\\d{2}-\\d{2}").split(phoneNumber);

        for (CodeOperator op: CodeOperator.values()) {
            if(code[1].equals(op.getCode())) {
                System.out.println(op.name().split("_\\d{1}")[0]);
            }
        }
    }

    private boolean checkPhone(String phone) {
        return Pattern.matches(NUMBER_FORMAT, phone);
    }
}
