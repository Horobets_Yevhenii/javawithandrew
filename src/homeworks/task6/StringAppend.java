package homeworks.task6;
/*
2)Соединить две строки след. вида:
("abc", "drf") результат: ("adbrcf"),
("ab", "drf") результат: ("adbrf),
("abc", "dr") результат: ("adbrc)
 */

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StringAppend {

    static void unknownMethod(char[] a, char[] b) {

        char[] c = new char[a.length + b.length];

        int ia = 0, ib = 0;

        while (ia + ib < c.length) {

            if (ia < a.length) {
                c[ia + ib] = a[ia];

                ia++;
            }

            if (ib < b.length) {
                c[ia + ib] = b[ib];

                ib++;
            }
        }

        for (char c1 : c) {
            System.out.print(c1);
        }
    }

    public static void main(String[] args) {

        String one = "abc";
        String two = "dr ";

        char[] firstArray = one.toCharArray();

        char[] secondArray = two.toCharArray();

        unknownMethod(firstArray, secondArray);

    }


//
//
//

    //ответ1 работает при равных строках
//        char[] c = new char[a.length + b.length];
//        for (int i = 0, j = 0; i < a.length; j += 2, i++) {
//            c[j] = a[i];
//            c[j + 1] = b[i];
//        }
//
//        for (char c1 : c) {
//
//            System.out.print(c1);
//        }

    //ответ3 оценить не могу - не понимаю
//    /** ''.join(map(add, a, b)) */
//    static String interleaved(String a, String b)
//    {
//        assert a.length() == b.length();
//        return IntStream.range(0, Math.min(a.length(), b.length()))
//                .mapToObj(i -> a.substring(i, i+1) + b.substring(i, i+1))
//                .collect(Collectors.joining());
//    }
    //ответ4 работает только с равными строками
//    static /*char[]*/ void interleaved(char[] a, char[] b) {
//        assert a.length == b.length;
//        char[] result = new char[a.length + b.length];
//        for (int i = 0; i < a.length; ++i) {
//            result[2 * i] = a[i];     // result[::2] = a
//            result[2 * i + 1] = b[i];   // result[1::2] = b
//        }
//        for (char c : result) {
//            System.out.print(c);
//        }
//    }
//}
//        return result;

    //ответ5 работает только с равными строками, но через StringBuilder
//    StringBuilder res = new StringBuilder();
//
//        for(int i=0; i<3; i++)
//        {
//        res.append(firstArray[i]);
//        res.append(secondArray[i]);
//        }
//
//        System.out.println(res.toString());

    static String mergeStrings(String a, String b) {//abc, df

        int l = Math.abs(b.length() - a.length());


        StringBuilder temp = new StringBuilder(a);

        temp = a.length() > b.length() ? new StringBuilder(b) : temp;

        for (int i = 0; i < l; i++) {

            temp.append(" ");
        }
//"abc" "df " -> "abcdf"

        StringBuilder result = new StringBuilder();

        for (int i = 0; i < b.length(); i++) {
            result.append(String.valueOf(a.charAt(i)).concat(String.valueOf(b.charAt(i))));
        }

        return result.toString().replaceAll(" ", "");
    }
}

