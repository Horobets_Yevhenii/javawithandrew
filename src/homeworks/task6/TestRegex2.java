package homeworks.task6;

import java.util.Scanner;

public class TestRegex2 {

    static Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Enter your phone number in the following format: (XXX)XXX-XX-XX");

        String number = SCANNER.next();

        PhoneNumber phoneNumber = new PhoneNumber();

        phoneNumber.findMatches(number);

    }
}
