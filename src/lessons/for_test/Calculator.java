package lessons.for_test;

import java.io.IOException;
import java.util.Random;

public class Calculator {

    public int sum(int value1, int value2) {
        return value1 + value2;
    }

    public boolean getNumber() {
        return true;
    }

    public void print() {
        System.out.println("Hello world");
    }

    public int diff() {
        System.out.println("Diff");

        return getValue();//5
    }

    public int getValue() {//hard logic
        return new Random().nextInt(20);
    }

    public final static int sumStatic() {
        return new Random().nextInt();
    }

    public int newSum() {
        return getRandomNumber();
    }

    private int getRandomNumber() {
        return new Random().nextInt();
    }

    public final int showNumber() {
        return new Random().nextInt();
    }

    public void throwException() throws IOException {
        throw new IOException("I'm IOE");
    }
}
