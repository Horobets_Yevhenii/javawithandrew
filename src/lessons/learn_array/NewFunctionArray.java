package lessons.learn_array;

public class NewFunctionArray {

    public static void main(String[] args) {

        int[] array = {4,5,6,7,8};

        int i = -1;

        while(i++ < array.length - 1){
            System.out.println(array[i]);
        }
    }
}
