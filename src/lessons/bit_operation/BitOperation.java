package lessons.bit_operation;

public class BitOperation {
    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(45));//00101101
        System.out.println(Integer.toBinaryString(13));//00001101
        System.out.println("45 & 13 = " + (45 & 13));     //00001101
        System.out.println("45 | 13 = " + (45 | 13));     //00101101

        System.out.println("45 << 2 = " + (45 << 2));     //00101101 -> 10110100
        //45 * 2(2) = 180
        //7 * 2(3) = 7 << 3

        //(int)Math.pow(7, 3);

        System.out.println("45 >> 2 = " + (45 >> 2));     //00101101 -> 00001011
        //45 / 2(2)

    }
}
