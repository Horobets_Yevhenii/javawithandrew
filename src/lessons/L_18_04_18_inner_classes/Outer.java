package lessons.L_18_04_18_inner_classes;

public class Outer {

    private int age;

    private static String name;

    public class Inner {
        public final static int firstName = 5;


        public void print() {
            System.out.println(age + name);
        }
    }

    protected static class StaticClass {

        public static String number = "";

        private int last_name;

        public void print() {
            System.out.println(name);
        }
    }

    public void learnLocalClass(int number) {

        /*for(;;) {
            class LocalClass {}
        }*/

        class Foo {

            public final static int hello = 0;

            public void print() {
                System.out.println(age + name + number);
            }
        }
    }

    interface Ballable {
        void jump();
    }

    public void learnAnonymousClass() {
        new Ballable() {
            @Override
            public void jump() {

            }
        };
    }


}

class TestInner {
    public static void main(String[] args) {
        Outer outer = new Outer();

        Outer.Inner inner = outer.new Inner();

        Outer.StaticClass staticClass = new Outer.StaticClass();
    }
}

/*
* Сначала инициализируются статические поля базового класса,
* Затем если есть статические поля в подклассе, инициализируются они.

При создании экземляра класса наследника инициализация начинается с обычных полей базового класса,
затем вызывается коструктор базового класса, после чего инициализируются обычные поля подкласса
и только после этого происходит вызов конструктора подкласса.
* */
