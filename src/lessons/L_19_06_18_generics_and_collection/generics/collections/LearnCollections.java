package lessons.L_19_06_18_generics_and_collection.generics.collections;

import java.util.ArrayList;
import java.util.Iterator;


public class LearnCollections {
    public static void main(String[] args) {
        ArrayList<Integer> integers = new ArrayList<>();

        //integers.trimToSize();

        integers.add(2);
        integers.add(6);
        integers.add(5);

        //System.out.println(integers); 1 2 3

        for(int elem: integers) {
           // System.out.println(elem);
        }

        Iterator<Integer> iterator = integers.iterator();

        while(iterator.hasNext()) {
            //System.out.println(iterator.next());
        }
    }
}
