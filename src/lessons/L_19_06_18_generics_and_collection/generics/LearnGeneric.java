package lessons.L_19_06_18_generics_and_collection.generics;

import java.util.ArrayList;
import java.util.List;

public class LearnGeneric {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        list.add(2);

        //list.add("String");

        /*for (Object i : list) {
            System.out.println(Integer.parseInt((String) i));
        }*/

    }
}

class SomeGeneric<T> {
    T[] array;

    public SomeGeneric(T[] newArray) {
        array = newArray;
    }

    public void printArray(){
        for (T t : array) {
            System.out.print(t + "\t");
        }
    }


}

class TestGeneric {
    public static void main(String[] args) {

        Double[] doubles = {.23, .56, .89};

        Integer[] ints = {1, 2, 3};

        SomeGeneric<Double> doubleGeneric = new SomeGeneric<>(doubles);

        SomeGeneric<Integer> integerGeneric = new SomeGeneric<>(ints);


        //doubleGeneric.printArray();
        integerGeneric.printArray();


    }
}
