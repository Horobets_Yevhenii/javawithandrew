package lessons.optional;

import java.util.Objects;
import java.util.Optional;

public class TryOptional {
    public static void main(String[] args) {
        Driver driver = new Driver("John", new Wheel(""));

        Car car = new Car(driver);

        System.out.println(car.getDriver().getFirstName());

        Driver driver1 = car.getDriver();

        /*if (Objects.nonNull(car) &&
                Objects.nonNull(driver1) &&
                Objects.nonNull(driver1.getWheel())) {
            System.out.println(driver1.getWheel().getModel());
        }*/

    }
}

class Driver {
    private String firstName;
    private Wheel wheel;

    public Driver(String firstName, Wheel wheel) {
        this.firstName = firstName;
        this.wheel = Optional.ofNullable(wheel).orElse(new Wheel("YOTO"));
    }

    public String getFirstName() {
        return firstName;
    }

    public void setWheel(Wheel wheel) {
        this.wheel = wheel;
    }

    public Wheel getWheel() {
        return wheel;
    }
}

class Car {
    private Driver  driver;

    public Car(Driver driver) {
        this.driver = Optional.ofNullable(driver).orElse(new Driver("Another driver", null));
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }
}

class Wheel {
    private String model;

    public Wheel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}

class UseOptional {
    public static void main(String[] args) {
        Optional<String> opt = Optional.empty();
        opt = Optional.ofNullable(null);
        //opt = Optional.of("Nike");

        //System.out.println(opt.isPresent());

        //opt.ifPresent(o -> System.out.println(o.toUpperCase()));

        //System.out.println(opt.get());

        System.out.println(opt.orElse("John"));
    }
}