package lessons.learn_class;

public class LearnClass {
    private String name;
    private int age;
    private static int INT = 0;



    public final String ADDRESS;

    {
        //ADDRESS = "45";
        System.out.println("Non-static block initialization");
    }


   static {

        System.out.println("Static block initialization");
    }

    public LearnClass(String name, int age) {
        ADDRESS = "45";
        System.out.println("Constructor");
        this.name = name;//aggregation
        this.name = new String("Hello");//composition
        this.age = age;
    }

    public static int getINT() {
        return INT;
    }

    public static void setINT(int INT) {
        //name = "";
        LearnClass.INT = INT;
    }

    public String getName() {
        int anInt = INT;
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {//(this, age)
        //if
        this.age = age;
    }
}

class TestClass {
    public static void main(String[] args) {
        String first = "First";

        int a = 10;

        LearnClass learnClass = new LearnClass(first, 12);

        LearnClass object1 = new LearnClass(first, 12);

        //object.setAge(24);//(this, 24)

        //object1.setAge(24);//(this, 24)

       /* LearnClass object = new LearnClass();


        object.setName("First");*/

        //System.out.println(object.getAge());
        System.out.println(LearnClass.getINT());
    }
}
