package lessons.pass_parameter;

public class Parameter {

    public static void changeInt(int a) {
        a = 15;
    }


    public static void changeString(String str) {
        str = "Run";//new String("Run")
    }

    public static void main(String[] args) {

        Passer passer = new Passer();

        String s = "Hello";

        changeString(s);

        int r = 20;

        changeInt(r);//


        System.out.println(r);
    }
}
