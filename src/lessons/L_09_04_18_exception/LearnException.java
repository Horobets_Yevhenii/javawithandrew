package lessons.L_09_04_18_exception;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLDataException;

public class LearnException {
    public static void main(String[] args) throws FileNotFoundException {
        //System.out.println(multiCatch1());

        workWithGeka();
    }

    public static void throwException() throws IOException {
        try {
            Files.createFile(Paths.get("./Geka/Hello.txt"));

            System.out.println("Hello");

        } catch (IOException e) {
            Files.createDirectory(Paths.get("./Geka"));
        }

        Files.write(Paths.get("./Geka/Hello.txt"), "Hello, Geka".getBytes());
    }

    public static void throwException1() throws IOException {

        throw new IOException("There isn't file");
    }

    public void someCaths() {
        try {
            if (1 == 1) {
                throw new IOException();
            } else if (2 != 2) {
                throw new Exception();
            } else {
                throw new SQLDataException();
            }
        }

        catch (IOException e) {

        } catch (SQLDataException e) {

        } catch (Exception e) {

        }
    }

    public static void multiCatch() {
        try {
            if (1 != 1) {
                throw new IOException();
            } else if(2 != 2) {
                throw new SQLDataException();
            }

            System.exit(5);

        } catch (IOException | SQLDataException e) {
            System.out.println("Inside catch clause");
        } finally {
            System.out.println("Inside finally");
        }
    }

    public static int multiCatch1() throws FileNotFoundException {
        try {
            if (1 != 1) {
                throw new IOException();
            } else if(2 != 2) {
                throw new SQLDataException();
            }

            //System.exit(5);

            return 5;

        } catch (IOException | SQLDataException e) {
            throw new FileNotFoundException();
        } finally {
            //System.out.println("Inside finally");
            return 40;
        }
    }

    public static void multiCatch2() throws IOException {
        try {
                throw new SQLDataException();
        }  finally {
            throw new IOException();
        }
    }

    public static void workWithGeka() {
        try {
            throw new GekaException("Hello");
        } catch (GekaException e) {
            e.printMessageFormGeka();
        }
    }

    public static void workWithGeka2() {
        try {
            throw new RuntimeException();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }
}

class  GekaException extends Exception {
     GekaException(String message) {
        super(message);
    }

    void printMessageFormGeka() {
        System.out.println("Hello from Geka ");
    }
}
