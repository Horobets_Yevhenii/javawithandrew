package lessons.learn_string;

public class LearnString {
    public static void main(String[] args) {
        String s = "Hello world";//object - literal
        String str = new String("Hello");//object

        /*System.out.println(s == str);

        System.out.println(s.equals(str));

        System.out.println(s == str.intern());//true

        System.out.println(s == str);

        System.out.println(1 + 2 + "3");//33

        System.out.println("1" + 2 + 3);//123*/

        //System.out.println(s.substring(0, 6));

        //System.out.println(s.contains("wo"));

        System.out.println(s.lastIndexOf('o'));

    }
}
