package lessons.learn_enum;

public class LearnEnum {
    public static void main(String[] args) {
        Planets earth = Planets.EARTH;

        Planets[] values = Planets.values();

        for (Planets pl : values) {
            System.out.println(pl);
        }

        String pl = "Upiter";

        Planets upiter = Planets.valueOf(pl.toUpperCase());

        System.out.println(Planets.EARTH.getShortName());

        String name = "FalL";

        for (Season s : Season.values())
            if (name.toUpperCase().equals(s.toString())) {
                System.out.println(/*s + ": " +*/"All right" + s.getDescription());
        }

        for (Season s : Season.values())
            System.out.println(s.ordinal() + ": " + s);
    }
}

enum Planets {
    EARTH("Earth"), UPITER("Upiter");

    private String shortName;

    Planets(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }
}

enum Season {
    WINTER("Cold season"),
    SPRING("Cool season"),
    SUMMER("Hot season"),
    FALL("Cool season");

    private String description;

    private Season(String description) {
        this.description = description;
    }

    public String getDescription() {return description;}
};
