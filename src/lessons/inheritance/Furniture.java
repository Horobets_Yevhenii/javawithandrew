package lessons.inheritance;

public class Furniture implements Softable {
    private String name;//has-a
    private int age;
    public static String firstName = "FirstName";

    public Furniture(String name, int age) {
        this.name = name;
        this.age = age;
    }

    String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void print() {
        System.out.println("Furniture");
    }

    @Override
    public void someRun() {
        System.out.println("Furniture run");
    }

    @Override
    public void run() {

    }
}

class Table extends Furniture {

    private int countLegs;

    public Table(String name, int age, int countLegs) {
        super(name, age);
        this.countLegs = countLegs;
    }
}

/*
* private - visibility only inside class
* package-private - visibility only inside package
* protected - visibility only inside package + childs
* public - visibility anywhere
* */