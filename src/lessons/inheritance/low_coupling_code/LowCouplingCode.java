package lessons.inheritance.low_coupling_code;


import com.sun.org.apache.bcel.internal.generic.NEW;

public class LowCouplingCode {

    private Operator operator;

    private OperatorOne operatorOne;

    public LowCouplingCode(Operator operator, OperatorOne operatorOne) {
        this.operator = operator;
        this.operatorOne = operatorOne;
    }

    public void action() {
        operator.print();
    }

    public void actionOne() {
        operatorOne.print();
    }
}

class NewLowCouplingCode {

    private Ballable ballable;

    public NewLowCouplingCode(Ballable ballable) {
        this.ballable = ballable;
    }

    public void action() {
        ballable.print();
    }

}


class Operator implements Ballable {

    @Override
    public void print() {
        System.out.println("Operator");
    }
}

class OperatorOne implements Ballable {
    public void print() {
        System.out.println("OperatorOne");
    }
}

class OperatorTwo implements Ballable {
    public void print() {
        System.out.println("OperatorTwo");
    }
}

interface Ballable {
    void print();
}

class TestCouplingCode {
    public static void main(String[] args) {
        Operator operator = new Operator();

        OperatorOne operatorOne = new OperatorOne();

        OperatorTwo operatorTwo = new OperatorTwo();

        NewLowCouplingCode code = new NewLowCouplingCode(operatorOne);

        code.action();
    }
}