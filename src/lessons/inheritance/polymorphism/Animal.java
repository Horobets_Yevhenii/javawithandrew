package lessons.inheritance.polymorphism;

import java.io.File;
import java.nio.file.Files;

public class Animal {
    public void say() {
        System.out.println("Say something");
    }

    Number getNumber() {
        return 5;
    }

    public File getFile() {
        return new File("");
    }

    private String getFile(String path) {
        return "";
    }

}

class Mouse extends Animal {
    @Override
    public void say() {
        System.out.println("Pi-pi-pi");
    }

    @Override
    public Integer getNumber() {
        return 10;
    }
}

class Lion extends Animal {
    @Override
    public void say() {
        System.out.println("R-r-r");
    }

    public void print() {

    }
}

class TestPolymorphism {

    public static void operationInt(int a) {
        System.out.println("int");
    }

    public static void operationInt(Integer a) {
        System.out.println("Integer");
    }

    public static void main(String[] args) {

        operationInt(new Integer(5));

        //int a = "";

        Animal animal = new Animal();

        Lion lion = new Lion();//new Animal

        Animal mouse = new Mouse();//new Animal

       /* Lion animal1 = (Lion) animal;

        animal1.print();*/


        //lion.say();

        //mouse.say();

        /*System.out.println(animal instanceof Animal);

        System.out.println(animal instanceof Lion);

        System.out.println(lion instanceof Animal);*/

    }
}
