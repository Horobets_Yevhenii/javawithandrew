package lessons.inheritance;

public class Sofa extends Furniture {//is-a

    private boolean isSoft;

    public Sofa(String name, int age, boolean isSoft) {
        super(name, age);
        this.isSoft = isSoft;
    }

    public boolean isSoft() {
        getName();
        return isSoft;
    }

    @Override
    public void print() {
        System.out.println("Sofa");
    }
}
