package lessons.inheritance;

import java.io.Serializable;

public interface Softable extends Runnable, Serializable {

    int NUMBER = 5;

    void someRun();

    default void foo() {
//        NUMBER = 7;
        System.out.println("Foo");
    }
}
