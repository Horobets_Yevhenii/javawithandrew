package lessons.wrappers;

public class Wrapper {
    public static void main(String[] args) {
        int a = 5;

        Integer b = 6;

        //b.intValue();

        a = b;//unboxing

        b = a;//autoboxing - new Integer(a)

        //a = new Object();

        //SOLID
    }
}
