package lessons.regex.pattern_matcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatcherClass {
    public static void main(String[] args) {
        String text = "This is text which is to be searched is for occurences of the word 'is'";

        String patternString = "is";

        Pattern pattern = Pattern.compile(patternString);

        Matcher matcher = pattern.matcher(text);

        while(matcher.find()) {
            System.out.println("found: " + matcher.group() +
                    "\nStart index " + matcher.start() +
                    "\nEnd index " + matcher.end());
            System.out.println("Method String " + text.substring(matcher.start(), matcher.end()));
        }
    }
}

class MatcherLookingAtMethod {
    public static void main(String[] args) {
        String text = "This is text which is to be searched is for occurences of the word 'is'";

        String patternString = "This is text";

        Pattern pattern = Pattern.compile(patternString);

        Matcher matcher = pattern.matcher(text);

        System.out.println("lookigAt = " + matcher.lookingAt());
        System.out.println("matches = " + matcher.matches());
    }
}
