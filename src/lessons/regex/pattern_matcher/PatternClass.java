package lessons.regex.pattern_matcher;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternClass {
    public static void main(String[] args) {
        //String text = "This is the text http:// pattern";
        String text = "This is the is text is http:// pattern";

        String pattern = ".*hTTp://.*";
//        String pattern = "is";

        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

        String[] split = p.split(pattern);

        Matcher matcher = p.matcher(text);

        System.out.println(matcher.matches());
    }
}
