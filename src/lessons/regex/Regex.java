package lessons.regex;

import java.util.regex.Pattern;

public class Regex {
    public static void main(String[] args) {
       /* String regex = "http://";

        String text = "http://google";

        //System.out.println(text.contains(regex));
        //System.out.println(Pattern.matches(regex, text));
        */

        //Matching any character

        /*String regex = ".";

        String text = "iu";

        System.out.println(Pattern.matches(regex, text));*/

        //More complex example with matching any character

        /*String regex = "W.rld";

        String text = "WTrld";

        System.out.println(Pattern.matches(regex, text));*/

        //Example with set any character

       /* String regex = "W[ai]rld";

        String text = "Wirld";

        System.out.println(Pattern.matches(regex, text));  */

        //Example with set any character in specific range

       /* String regex = "W[A-Za-z]rld";// \\w -> [A-Za-z_0-9]

        String text = "Wirld";

        System.out.println(Pattern.matches(regex, text));*/

        //Example with digit

        /*String regex = "W\\drld";//  \d

        String text = "W8rld";

        System.out.println(Pattern.matches(regex, text)); */

        //Example with  range of digits

        /*String regex = "W[0-8]rld";//  \d

        String text = "W8rld";

        System.out.println(Pattern.matches(regex, text)); */

        //Example with  non-digits

        //String regex = "W[^0-9]rld";
        /*String regex = "W\\Drld";

        String text = "World";

        System.out.println(Pattern.matches(regex, text));
        */
    //Example with matching any character in word

        /*String regex = "W\\rld";

        String text = "W8rld";

        System.out.println(Pattern.matches(regex, text));*/

    //Example with matching any non-character in word

        //String regex = "W[^A-Za-z_0-9]rld";// \\W
    //    String regex = "W\\Wrld";// \\W

    //    String text = "W@rld";

    //    System.out.println(Pattern.matches(regex, text));

    }
}

class Quantifiers {
    public static void main(String[] args) {
//        The * quantifiers, from zero to many character

        /*String regex = "World*";

        String text = "World";

        System.out.println(Pattern.matches(regex, text));*/

        //The + quantifiers, from 1 to many character

        /*String regex = "World+";

        String text = "World"

        System.out.println(Pattern.matches(regex, text));*/

        //The {n} quantifiers, exactly {n} times

        /*String regex = "World{3}";

        String text = "Worlddd";

        System.out.println(Pattern.matches(regex, text));*/

        //The {n, r} quantifiers, exactly  from{n} to {r} times

        /*String regex = "World{2,4}";

        String text = "World";

        System.out.println(Pattern.matches(regex, text));*/

        //The ? quantifiers, 0 or 1 times

//        String regex = "World?";
//
//        String text = "Worl";

//        System.out.println(Pattern.matches(regex, text));
    }
}
