package lessons.regex;

import java.util.regex.Pattern;

public class OrCondition {
    public static void main(String[] args) {
        String string = "World run";

        String regex = ".*(run|gun).*";

        System.out.println(Pattern.matches(regex, string));
    }
}
